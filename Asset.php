<?php

/**
 * The Asset class of the test task by Stellarbit company.
 *
 * @author     Iakupov Iunir <inforser@yandex.ru>
 * @version    1
 */

class Asset {

    /**
     * The globally used array of generated asset identifiers.
     *
     * @var array
     */
	protected static $generated_ids = [];

    /**
     * The unique identifier of asset.
     *
     * @var integer
     */
    protected $id;

    /**
     * The name of asset.
     *
     * @var string
     */
    protected $name;

    /**
     * The price of asset.
     *
     * @var integer
     */
    protected $price;

	/**
	 * Asset class constructor
	 *
	 * Create a new Asset instance. Unique identifier should be generated randomly.
	 *
	 * @param  string $name  The asset name. Cannot be passed is null, empty or has more than 100 characters.
	 * @param  float  $price The asset price. Cannot be less or equal to 0.
     * @throws Exeption
	 */
    public function __construct($name, $price) {
    	if(is_null($name) || strlen($name)>100 || strlen($name)==0) {
    		throw new Exception('The name should not be passed as null, empty or has more than 100 characters');
    	}
    	if($price<=0) {
    		throw new Exception('The price should be more than 0');
    	}

        $this->name = $name;
        $this->price = $price;
        $this->id = $this->uniqueIdGenerator();
    }

    /**
     * Access to asset parameters.
     *
     * @param  string  $name
     * @return mixed
     */
    public function __get($name) {
	    return isset($this->$name) ? $this->$name : null;
	}

    /**
     * Generate unique identifier to asset.
     *
     * @return integer
     */
    protected function uniqueIdGenerator() {
		do {
		    $id = rand(1, PHP_INT_MAX);
		} while (in_array($id, self::$generated_ids));
		self::$generated_ids[] = $id;
		return $id;
    }
}