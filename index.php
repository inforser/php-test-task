<?php 

include_once "Asset.php";
include_once "Company.php";

/**
 * Generating array of assets
 */
$assets = [];
for($i=1; $i<5; $i++)
{
	$assets[] = new Asset("Asset number ".$i, rand(1, 10000));
}

/**
 * Making a company instance
 */
$company = new Company("Some name", 1, 100, $assets);
echo "New company:<br>";
var_dump($company);
echo '<br><br>';

/**
 * Hiring employee
 */
echo "The number of the company employees before hire: " . $company->number_of_employees . "<br>";
$company->hireEmployee();
echo "The number of the company employees after hire: " . $company->number_of_employees . "<br>";
echo '<br><br>';

/**
 * Firing employes
 */
$company->fireEmployee();
echo "The number of the company employees after fire: " . $company->number_of_employees . "<br>";

//Throws Exeption
// $company->fireEmployee();
echo '<br><br>';

/**
 * Buying asset
 */
$asset = new Asset("Some new asset", 50);
var_dump($asset);
echo '<br>';
echo "The number of the company assets before buying new asset: " . count($company->assets) . "<br>";
echo "The company balance before buying new asset: " . $company->balance . "<br>";
$company->buyAsset($asset);
echo "The number of the company assets after buying new asset: " . count($company->assets) . "<br>";
echo "The company balance after buying new asset: " . $company->balance . "<br>";

//Throws Exeption
// $expensive_asset = new Asset("Some expensive asset", 500);
// $company->buyAsset($expensive_asset);
echo '<br><br>';

/**
 * Selling asset
 */
$company->sellAsset($asset->id);
echo "The number of the company assets after selling asset: " . count($company->assets) . "<br>";
echo "The company balance after selling asset: " . $company->balance . "<br>";

//Throws Exeption
// $external_asset = new Asset("Some external asset", 10);
// $company->sellAsset($external_asset->id);

