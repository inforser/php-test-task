<?php

/**
 * The Company class of the test task by Stellarbit company.
 *
 * @author     Iakupov Iunir <inforser@yandex.ru>
 * @version    1
 */

include_once "Asset.php";

class Company {

    /**
     * The globally used array of generated company identifiers.
     *
     * @var array
     */
	protected static $generated_ids = [];

    /**
     * The unique identifier of company.
     *
     * @var integer
     */
    protected $id;

    /**
     * The name of company.
     *
     * @var string
     */
    protected $name;

    /**
     * The number of employees of company.
     *
     * @var integer
     */
    protected $number_of_employees;

    /**
     * The balance of company.
     *
     * @var integer
     */
    protected $balance;

    /**
     * The assets of company.
     *
     * @var array
     */
    protected $assets;

    /**
     * Company class constructor
     *
     * Create a new Company instance. Unique identifier should be generated randomly.
     *
     * @param  string   $name                The name of the company. Cannot be passed is null, empty or has more than 100 characters.
     * @param  integer  $number_of_employees The number of employees in the company.
     * @param  float    $balance             The balance of the company. Cannot be less than 0.
     * @param  array    $assets              The assets of the company. Should be array of Asset instances.
     * @throws Exeption                      If params are incorrect.
     */
    public function __construct($name, $number_of_employees = 1, $balance = 0, $assets = []) {
    	if(is_null($name) || strlen($name)>100 || strlen($name)==0) {
    		throw new Exception('The name should not be passed as null, empty or has more than 100 characters');
    	}
    	if($balance<0) {
    		throw new Exception('The balance should not be less than 0');
    	}
        if(is_array($assets))
        {
            foreach ($assets as $asset) {
                if(!$asset instanceof Asset) {
                    throw new Exception('The assets should be array of Asset instances');
                }
            }
        } else {
            throw new Exception('The assets should be array');
        }
        $this->name = $name;
        $this->number_of_employees = $number_of_employees;
        $this->balance = $balance;
        $this->assets = $assets;
        $this->id = $this->uniqueIdGenerator();
    }

    /**
     * Hire an employee.
     *
     * @return void
     */
    public function hireEmployee() {
        $this->number_of_employees++;
    }
    
    /**
     * Fire an employee.
     *
     * @return void
     * @throws Exeption If after execution company will have 0 employees.
     */
    public function fireEmployee() {
        if($this->number_of_employees>1)
        {
            $this->number_of_employees--;
        } else {
            throw new Exception('The company can not have 0 employees');
        }
    }

    /**
     * Buy an asset.
     *
     * @param  Asset    $asset The asset to buy.
     * @return void
     * @throws Exeption        If asset price more than company balance.
     */
    public function buyAsset($asset) {
        if($asset->price > $this->balance) {
            throw new Exception('The company cannot buy this asset. The balance should not be less than 0');
        } else {
            $this->balance -= $asset->price;
            $this->assets[] = $asset;
        }
    }

    /**
     * Buy an asset.
     *
     * @param  integer  $asset_id The id of asset to sell.
     * @return void
     * @throws Exeption           If company does not have this asset.
     */
    public function sellAsset($asset_id) {
        foreach ($this->assets as $key => $asset) {
            if($asset->id == $asset_id)
            {
                $this->balance += $asset->price;
                array_splice($this->assets, $key, 1);
                return;
            }
        }
        throw new Exception('The company does not have this asset');
    }

    /**
     * Access to company parameters.
     *
     * @param  string  $name
     * @return mixed
     */
    public function __get($name) {
        return isset($this->$name) ? $this->$name : null;
    }

    /**
     * Generate unique identifier to company.
     *
     * @return integer
     */
    protected function uniqueIdGenerator() {
		do {
		    $id = rand(1, PHP_INT_MAX);
		} while (in_array($id, self::$generated_ids));
		self::$generated_ids[] = $id;
		return $id;
    }
}